#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <tbb/tbb.h>
#include <openssl/md5.h>

using namespace tbb;
const char* chars="0123456789";

class findKey {
char* arg;
public:
findKey(char* setArg){arg = setArg;}
// tests if a hash matches a candidate password
int test(const char* passhash, const char* passcandidate) const {
    unsigned char digest[MD5_DIGEST_LENGTH];
    
    MD5((unsigned char*)passcandidate, strlen(passcandidate), digest);
    
    char mdString[34];
    mdString[33]='\0';
    for(int i=0; i<16; i++) {
        sprintf(&mdString[i*2], "%02x", (unsigned char)digest[i]);
    }
    return strncmp(passhash, mdString, strlen(passhash));
}

// maps a PIN to a string
void genpass(long passnum, char* passbuff) const {
    passbuff[8]='\0';
    int charidx;
    int symcount=strlen(chars);
    for(int i=7; i>=0; i--) {
        charidx=passnum%symcount;
        passnum=passnum/symcount;
        passbuff[i]=chars[charidx];
    }
}

void operator()(const blocked_range<long>& r) const {
    for (int i=r.begin(); i!=r.end(); i++) {
        char passmatch[9];
	genpass(i, passmatch);
        int notfound = test(arg, passmatch);
        if (notfound == 0) {
            printf("found: %s\n", passmatch);
        }
    }
}
};

int main(int argc, char** argv) {
    if(argc != 2) {
        printf("Usage: %s <password hash>\n",argv[0]);
        return 1;
    }
    findKey find(argv[1]);
    parallel_for( blocked_range<long>(0, 999999), find );
    return 0;
}
