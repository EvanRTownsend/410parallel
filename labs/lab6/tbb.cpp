// While not particulary secure... it turns out this strategy also isn't too 
// bad (as long as the keys are kept secret and have different lengths and 
// aren't reused together)... or at least that is what a member of the security
// group that works in crypto told me.

#include "key.h"
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <tbb/tbb.h>

using namespace tbb;

class Encoder {
public:
    char cipherChar;
    xorKey* keyList;
    int charLoop;
    
    // Initial Constructor
    Encoder(char setCipherChar, xorKey* setKeyList, int setCharLoop) {
        cipherChar = setCipherChar;
        keyList = setKeyList;
        charLoop = setCharLoop;
    }
    
    // Split constructor. Required by TBB
    Encoder(Encoder& x, split) {
        printf("helloooooo\n");
        cipherChar = x.cipherChar;
        keyList = x.keyList;
        charLoop = x.charLoop;
    }
    
    // Joins two instances of Encoder. Required by TBB
    void join(const Encoder x) {
        cipherChar = cipherChar ^ x.cipherChar;
    }

    void operator() (const blocked_range<int>& r) {
        for(int i = r.begin(); i != r.end(); i ++) {
            cipherChar = cipherChar ^ getBit(&keyList[i], charLoop);
        }
    }
};

// utility function: given a list of keys, a list of files to pull them from, 
// and the number of keys -> pull the keys out of the files, allocating memory 
// as needed
void getKeys(xorKey* keyList, char** fileList, int numKeys)
{
  int keyLoop=0;
  for(keyLoop=0;keyLoop<numKeys;keyLoop++)
  {
     readKey(&(keyList[keyLoop]), fileList[keyLoop]);
  }
}
//Given text, a list of keys, the length of the text, and the number of keys, encodes the text
void encode(char* plainText, char* cypherText, xorKey* keyList, int ptextlen, int numKeys) {
  int keyLoop=0;
  int charLoop=0;
  for(charLoop=0;charLoop<ptextlen;charLoop++) {
    char cipherChar=plainText[charLoop]; 
    Encoder encoder(cipherChar, keyList, charLoop);
    parallel_reduce( blocked_range<int>(0, numKeys), encoder);
    cypherText[charLoop]=encoder.cipherChar;
    printf("%c", cypherText[charLoop]);
  }
  printf("\n");
}

void decode(char* cypherText, char* plainText, xorKey* keyList, int ptextlen, int numKeys) {
  encode(cypherText, plainText, keyList, ptextlen, numKeys); //isn't symmetric key cryptography awesome? 
}

int main(int argc, char* argv[]) {
  if(argc<=2)
  {
      printf("Usage: %s <fileToEncrypt> <key1> <key2> ... <key_n>\n",argv[0]);
      return 1;
  }

  // read in the keys
  int numKeys=argc-2;
  xorKey* keyList=(xorKey*)malloc(sizeof(xorKey)*numKeys); // allocate key list
  getKeys(keyList,&(argv[2]),numKeys);
  
  // read in the data to encrypt/decrypt
  off_t textLength=fsize(argv[1]); //length of our text
  FILE* rawFile=(FILE*)fopen(argv[1],"rb"); //The intel in plaintext
  char* rawData = (char*)malloc(sizeof(char)*textLength);
  fread(rawData,textLength,1,rawFile);
  fclose(rawFile);

  // Encrypt
  char* cypherText = (char*)malloc(sizeof(char)*textLength);
  encode(rawData,cypherText,keyList,textLength,numKeys);

  // Decrypt
  char* plainText = (char*)malloc(sizeof(char)*textLength);
  decode(cypherText,plainText,keyList,textLength,numKeys);

  // write out
  FILE* encryptedFile=(FILE*)fopen("encryptedOut","wb");
  FILE* decryptedFile=(FILE*)fopen("decryptedOut","wb");

  fwrite(cypherText,textLength,1,encryptedFile);
  fwrite(plainText,textLength,1,decryptedFile);

  fclose(encryptedFile);
  fclose(decryptedFile);

  // Check
  int i;
  for(i=0;i<textLength;i++) {
    if(rawData[i]!=plainText[i]) {
      printf("Encryption/Decryption is non-deterministic\n");
      i=textLength;
    }
  }

  return 0;

}
